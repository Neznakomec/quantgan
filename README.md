# Quant GANs replication

[paper](https://arxiv.org/abs/1907.06673)

## Notebook files
[sp500_training_torch.ipynb] - replication of **Pure TCN** way from article, on S&P 500 dataset.

In that work next sources were used:
* [TCN implementation on PyTorch](https://github.com/locuslab/TCN/blob/master/TCN/tcn.py)
* [QuantGAN implementation and researches on TensorFlow framework](https://github.com/ICascha/QuantGANs-replication)