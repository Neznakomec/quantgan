import torch.nn as nn
import numpy as np
import torch.optim


class GAN:
    """ Generative adversarial network class.

    Training code for a standard DCGAN using the Adam optimizer.
    Code taken in part from: https://github.com/tensorflow/docs/blob/master/site/en/tutorials/generative/dcgan.ipynb
    https://pytorch.org/tutorials/beginner/dcgan_faces_tutorial.html
    """

    def discriminator_loss(self, real_output, fake_output):
        real_loss = self.loss(real_output,
                              torch.ones_like(real_output))
        fake_loss = self.loss(fake_output,
                              torch.zeros_like(fake_output))
        total_loss = real_loss + fake_loss
        return total_loss

    def generator_loss(self, fake_output):
        return self.loss(fake_output, torch.ones_like(fake_output))

    def __init__(self, discriminator, generator, training_input, lr_d=1e-4, lr_g=3e-4, epsilon=1e-8, beta_1=.0,
                 beta_2=0.9, generator_input_shape=None):
        """Create a GAN instance

        Args:
            discriminator (tensorflow.keras.models.Model): Discriminator model.
            generator (tensorflow.keras.models.Model): Generator model.
            training_input (int): input size of temporal axis of noise samples.
            lr_d (float, optional): Learning rate of discriminator. Defaults to 1e-4.
            lr_g (float, optional): Learning rate of generator. Defaults to 3e-4.
            epsilon (float, optional): Epsilon parameter of Adam. Defaults to 1e-8.
            beta_1 (float, optional): Beta1 parameter of Adam. Defaults to 0.
            beta_2 (float, optional): Beta2 parameter of Adam. Defaults to 0.9.
        """
        self.discriminator = discriminator
        self.generator = generator
        self.noise_shape = [generator_input_shape[0], training_input, generator_input_shape[-1]]

        self.loss = nn.BCELoss()

        self.generator_optimizer = torch.optim.Adam(lr=lr_g, eps=epsilon, betas=(beta_1, beta_2),
                                                    params=generator.parameters())
        self.discriminator_optimizer = torch.optim.Adam(lr=lr_d, eps=epsilon, betas=(beta_1, beta_2),
                                                        params=discriminator.parameters())

    def train(self, data, batch_size, n_batches, additional_d_steps, device):
        """training function of a GAN instance.
        Args:
            data (4d array): Training data in the following shape: (samples, n_series, time_steps, 1).
            batch_size (int): Batch size used during training.
            n_batches (int): Number of update steps taken.
            additional_d_steps (int): Number of extra discriminator training steps during each update.
            device: device where to execute training process
        """

        for n_batch in range(n_batches):
            if n_batch % 100 == 0:
                print(f'processing epoch = {n_batch}')
            # sample uniformly
            batch_idx = np.random.choice(np.arange(data.shape[0]), size=batch_size,
                                         replace=(batch_size > data.shape[0]))
            batch = data[batch_idx]
            batch = torch.from_numpy(batch)
            batch.to(device)

            self.train_step(batch, batch_size, additional_d_steps)

            self.train_hook(n_batch)

    def train_step(self, data, batch_size, additional_d_steps):

        self.discriminator.train()
        self.generator.eval()

        for _ in range(additional_d_steps + 1):
            noise = torch.randn([batch_size, *self.noise_shape])
            generated_data = self.generator(noise)

            self.discriminator.zero_grad()

            real_output = self.discriminator(data)
            fake_output = self.discriminator(generated_data)
            disc_loss = self.discriminator_loss(real_output, fake_output)

            disc_loss.backward()
            self.discriminator_optimizer.step()

        self.generator.zero_grad()

        noise = torch.randn([batch_size, *self.noise_shape])

        self.generator.train()
        generated_data = self.generator(noise)
        self.discriminator.eval()
        fake_output = self.discriminator(generated_data)
        gen_loss = self.generator_loss(fake_output)
        gen_loss.backward()

        self.generator_optimizer.step()

    def train_hook(self, n_batch):
        """Override this method to insert behaviour at every training update.
           Access to the instance namespace is provided.
        """
        pass
