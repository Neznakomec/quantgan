import torch
import torch.nn as nn
from torch.nn.utils import weight_norm
from backend.tcn import TemporalConvNet

'''
Helper methods for making TCNs for Discriminator and Generator
in GAN architecture
'''


class PrepareGeneratorFor1dTCN(nn.Module):
    def __init__(self, in_features, out_features):
        super(PrepareGeneratorFor1dTCN, self).__init__()
        self.linear = nn.Linear(in_features=in_features, out_features=out_features * 2)
        self.conv = torch.nn.Conv1d(in_channels=3, out_channels=2, kernel_size=(2,), padding=(1,), dilation=out_features + 1)

    def forward(self, x):
        # x.shape (batch_size, n_series = 1, train_input = 253 for example, n_features = 3)
        x = torch.swapaxes(x, 1, 3)  # (batch, 1, 253, 3) -> (batch, 3, 253, 1)
        x = x.squeeze(-1)  # (batch, 3, 253, 1) -> (batch, 3, 253)
        x = self.conv.forward(x)  # (batch, 3, 253, 1) -> (batch, 2, 253 - rfs + 1)
        return x


class PrepareGeneratorAnswerAfter1dTCN(nn.Module):
    def __init__(self, in_features, out_features):
        super(PrepareGeneratorAnswerAfter1dTCN, self).__init__()
        self.conv = weight_norm(
            nn.Conv1d(in_channels=in_features, out_channels=in_features, kernel_size=(1,))
        )
        self.conv2 = weight_norm(
            nn.Conv1d(in_channels=in_features, out_channels=out_features, kernel_size=(1,))
        )
        self.relu = nn.ReLU(inplace=True)
        self.relu2 = nn.ReLU(inplace=True)

    def forward(self, x):
        # x.shape (batch_size, n_filters = 100, train_input - rfs + 1)
        x = self.relu(x)
        x = self.conv(x)  # data will have same shape (batch_size, n_filters = 100, train_input - rfs + 1)
        x = self.relu2(x)
        x = self.conv2(x)  # reduce n_filters to one channel (batch_size, 1, train_input - rfs + 1)
        x = x.unsqueeze(3)  # just make a shape as in original:(batch_size, 1, train_input - rfs + 1, 1)
        return x


class PrepareDiscriminatorFor1dTCN(nn.Module):
    def __init__(self, in_features, out_features, block_size):
        super(PrepareDiscriminatorFor1dTCN, self).__init__()
        self.block_size = block_size
        self.linear = nn.Linear(in_features=in_features, out_features=out_features * block_size)

    def forward(self, x):
        # x.shape (batch_size, n_series = 1, train_input - rfs + 1, 1)
        x = x.squeeze()  # (64, 1, 127, 1) -> (64, 127)
        x = self.linear(x)  # (64, 127) -> (batch_size, (train_input - rfs + 1) * block_size)
        x = x.reshape(-1, x.shape[-1] // self.block_size,
                      self.block_size)  # (batch_size, (train_input - rfs + 1), block_size)
        return x


class PrepareDiscriminatorAnswerAfter1dTCN(nn.Module):
    def __init__(self, in_features, out_features):
        super(PrepareDiscriminatorAnswerAfter1dTCN, self).__init__()
        self.conv = weight_norm(
            nn.Conv1d(in_channels=in_features, out_channels=in_features, kernel_size=(2,))
        )
        self.conv2 = weight_norm(
            nn.Conv1d(in_channels=in_features, out_channels=out_features, kernel_size=(1,))
        )
        self.relu = nn.ReLU(inplace=True)
        self.relu2 = nn.ReLU(inplace=True)

    def forward(self, x):
        # x.shape (batch_size, n_filters, block_size)
        x = self.relu(x)
        x = self.conv(x)  # (batch_size, n_filters, block_size) -> (batch_size, n_filters, 1)
        x = self.relu2(x)
        x = self.conv2(x)  # (batch_size, n_filters, 1) -> (batch_size, 1, 1)
        x = torch.clamp(x, min=0, max=1)
        x = x.unsqueeze(3)  # just make a shape as in original: (batch_size, 1, 1, 1)
        return x


def make_Generator_TCN(dilations, fixed_filters, input_dim, block_size=2):
    """Creates a causal temporal convolutional network with skip connections.
       This network uses 2D convolutions in order to model multiple timeseries co-dependency.
    Args:
        dilations (list, tuple): Ordered number of dilations to use for the temporal blocks.
        fixed_filters (int): Number of channels in the hidden layers corresponding fixed over series axis.
        input_dim (list, tuple): Input dimension of the shape (number of timeseries, timesteps, number of features). Timesteps may be None for variable length timeseries.
        block_size (int): How many convolution layers to use within a temporal block. Defaults to 2.
    Returns:
        torch.nn.Sequential: a non-compiled model with TCN and some "adaptation" of the input and output
    """

    rfs = receptive_field_size(dilations, block_size)
    n_series = input_dim[0]

    if n_series != 1:
        raise ValueError("sorry, i'm not prepared to work with multiple series at once. Hope it will be done in future")

    return nn.Sequential(
        PrepareGeneratorFor1dTCN(in_features=fixed_filters, out_features=rfs),
        TemporalConvNet(
            num_inputs=block_size,
            num_channels=[fixed_filters] * len(dilations),
            kernel_size=block_size,
            dropout=0.2
        ),
        PrepareGeneratorAnswerAfter1dTCN(in_features=fixed_filters, out_features=n_series),
    )


def make_Discriminator_TCN(dilations, fixed_filters, input_dim,
                           block_size=2):
    """Creates a causal temporal convolutional network with skip connections.
       This network uses 2D convolutions in order to model multiple timeseries co-dependency.
    Args:
        dilations (list, tuple): Ordered number of dilations to use for the temporal blocks.
        fixed_filters (int): Number of channels in the hidden layers corresponding fixed over series axis.
        input_dim (list, tuple): Input dimension of the shape (number of timeseries, timesteps, number of features). Timesteps may be None for variable length timeseries.
        block_size (int): How many convolution layers to use within a temporal block. Defaults to 2.
    Returns:
        torch.nn.Sequential: a non-compiled model with TCN and some "adaptation" of the input and output
    """
    rfs = receptive_field_size(dilations, block_size)
    n_series = input_dim[0]

    if n_series != 1:
        raise ValueError("sorry, i'm not prepared to work with multiple series at once. Hope it will be done in future")

    return nn.Sequential(
        PrepareDiscriminatorFor1dTCN(in_features=rfs, out_features=rfs, block_size=block_size),
        TemporalConvNet(
            num_inputs=rfs,
            num_channels=[fixed_filters] * len(dilations),
            kernel_size=block_size,
            dropout=0.2
        ),
        PrepareDiscriminatorAnswerAfter1dTCN(in_features=fixed_filters, out_features=n_series)
    )


def receptive_field_size(dilations, block_size):
    """Non-exhaustive computation of receptive field size.
    Args:
        dilations (list, tuple): Ordered number of dilations of the network.
        block_size (int): Number of convolution layers in each temporal block of the network.
    Returns:
        int: the receptive field size.
    """
    return 1 + block_size * sum(dilations)
